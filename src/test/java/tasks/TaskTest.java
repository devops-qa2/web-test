package tasks;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverConditions.urlContaining;

import org.junit.jupiter.api.Test;

public class TaskTest extends Hooks{

    @Test
    public void testSalvarTaskComSucesso(){
        open("http://192.168.0.9:8001/tasks/");
        $("#addTodo").shouldBe(visible).click();
        webdriver().shouldHave(urlContaining("/tasks/add"));
        $("#task").shouldBe(visible).setValue("task teste");
        $("#dueDate").setValue("10/10/2024");
        $("#saveButton").click();
        webdriver().shouldHave(urlContaining("/tasks/save"));
        $("#message")
                .shouldBe(visible)
                .shouldBe(text("Success!"));
    }

    @Test
    public void testTentarSalvarTaskComDescricaoVazia(){
        open("http://192.168.0.9:8001/tasks/add");
        $("#dueDate").setValue("10/10/2024");
        $("#saveButton").click();
        $("#message")
                .shouldBe(visible)
                .shouldBe(text("Fill the task description"));

    }


    @Test
    public void testTentarSalvarTaskComDataVazia(){
        open("http://192.168.0.9:8001/tasks/add");
        $("#task").shouldBe(visible).setValue("task teste");
        $("#saveButton").click();
        $("#message")
                .shouldBe(visible)
                .shouldBe(text("Fill the due date"));
    }


    @Test
    public void testTentarSalvarTaskComDataPassada(){
        open("http://192.168.0.9:8001/tasks/add");
        $("#task").shouldBe(visible).setValue("task teste");
        $("#dueDate").setValue("10/10/2014");
        $("#saveButton").click();
        $("#message")
                .shouldBe(visible)
                .shouldBe(text("Due date must not be in past"));

    }
}
