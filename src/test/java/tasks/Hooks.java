package tasks;

import com.codeborne.selenide.Configuration;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

import static com.codeborne.selenide.Selenide.closeWebDriver;

public class Hooks {

    @AfterEach
    public void tearDown(){
        closeWebDriver();
    }
}
